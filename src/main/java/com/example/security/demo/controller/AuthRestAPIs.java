package com.example.security.demo.controller;

import com.example.security.demo.model.auth.request.LoginForm;
import com.example.security.demo.model.auth.request.SignUpForm;
import com.example.security.demo.service.AuthService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class AuthRestAPIs {

    private final AuthService authService;

    @Autowired
    public AuthRestAPIs(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/api/auth/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {
        return ResponseEntity.ok(authService.authenticateUser(loginRequest));
    }

    @PostMapping("/api/auth/signup")
    public ResponseEntity<String> registrationUser(@Valid @RequestBody SignUpForm signUpRequest) {
        return authService.registrationUser(signUpRequest);
    }
}
