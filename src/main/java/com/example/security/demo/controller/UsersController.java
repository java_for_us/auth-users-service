package com.example.security.demo.controller;

import com.example.security.demo.model.dto.UserTransfer;
import com.example.security.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsersController {

  private final UserService userService;

  @Autowired
  public UsersController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping("/api/users")
    public ResponseEntity<Iterable<UserTransfer>> getAll(){
       Iterable<UserTransfer> users = userService.getUsers();
       return ResponseEntity.ok(users);
    }
}
