package com.example.security.demo.repository.postres;

import com.example.security.demo.model.Role;
import com.example.security.demo.model.RoleName;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}