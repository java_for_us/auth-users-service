package com.example.security.demo.service.impl;

import com.example.security.demo.model.User;
import com.example.security.demo.model.dto.UserTransfer;
import com.example.security.demo.repository.postres.UserRepository;
import com.example.security.demo.service.UserService;
import com.example.security.demo.service.converter.UserToUserLikeAuthorTransferConverter;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final UserToUserLikeAuthorTransferConverter userToUserLikeAuthorTransferConverter;

    @Autowired
    public UserServiceImpl(
        UserRepository userRepository,
        UserToUserLikeAuthorTransferConverter userToUserLikeAuthorTransferConverter) {
        this.userRepository = userRepository;
        this.userToUserLikeAuthorTransferConverter = userToUserLikeAuthorTransferConverter;
    }

    @Override
    public Iterable<UserTransfer> getUsers(){
        List<User> userList = userRepository.findAll();

       return userList.stream().map(userToUserLikeAuthorTransferConverter::convert)
            .collect(Collectors.toList());
    }
}
