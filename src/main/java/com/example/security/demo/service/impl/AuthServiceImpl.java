package com.example.security.demo.service.impl;

import com.example.security.demo.messaging.UsersCacheResetSender;
import com.example.security.demo.model.Role;
import com.example.security.demo.model.RoleName;
import com.example.security.demo.model.RolePrefix;
import com.example.security.demo.model.User;
import com.example.security.demo.model.auth.request.LoginForm;
import com.example.security.demo.model.auth.request.SignUpForm;
import com.example.security.demo.model.auth.response.JwtResponse;
import com.example.security.demo.repository.postres.RoleRepository;
import com.example.security.demo.repository.postres.UserRepository;
import com.example.security.demo.service.AuthService;
import com.example.security.demo.service.JwtProvider;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class AuthServiceImpl implements AuthService {

    private final AuthenticationManager authenticationManager;

    private final JwtProvider jwtProvider;

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder encoder;

    private final UsersCacheResetSender usersCacheResetSender;

    @Autowired
    public AuthServiceImpl(
        AuthenticationManager authenticationManager,
        JwtProvider jwtProvider,
        UserRepository userRepository,
        RoleRepository roleRepository,
        PasswordEncoder encoder,
        UsersCacheResetSender usersCacheResetSender) {
        this.authenticationManager = authenticationManager;
        this.jwtProvider = jwtProvider;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
        this.usersCacheResetSender = usersCacheResetSender;
    }

    @Override
    public JwtResponse authenticateUser(LoginForm loginRequest){
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateJwtToken(authentication);

        return new JwtResponse(jwt);
    }

    @Override
    public ResponseEntity<String> registrationUser(SignUpForm signUpRequest){

        User user = User.builder()
            .name(signUpRequest.getName())
            .username(signUpRequest.getUsername())
            .email(signUpRequest.getEmail())
            .password(encoder.encode(signUpRequest.getPassword()))
            .description(signUpRequest.getDescription())
            .createdDate(ZonedDateTime.now())
            .updatedDate(ZonedDateTime.now())
            .build();
        // user is required
        Set<String> strRoles = signUpRequest.getRoles();
        strRoles.add("ROLE_USER");
        Set<Role> roles = initRoles(strRoles);

        user.setRoles(roles);
        userRepository.save(user);
        usersCacheResetSender.sendEventToClearCacheUsers();
        return ResponseEntity.ok().body("User registered successfully!");
    }

    private Set<Role> initRoles(Set<String> strRoles) {
        Set<Role> roles = new HashSet<>();
        strRoles.forEach(role -> {
            if (role.equals(RolePrefix.admin.toString())) {
                Optional<Role> roleAdmin = roleRepository.findByName(RoleName.ROLE_ADMIN);
                roles.add(roleAdmin.get());
            }

            if (role.equals(RolePrefix.user.toString())) {
                Optional<Role> roleUser = roleRepository.findByName(RoleName.ROLE_USER);
                roles.add(roleUser.get());
            }
        });

        return roles;
    }
}
