package com.example.security.demo.service;

import com.example.security.demo.model.dto.UserTransfer;

public interface UserService {
    Iterable<UserTransfer> getUsers();
}
