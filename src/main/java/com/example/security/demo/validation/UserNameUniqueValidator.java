package com.example.security.demo.validation;

import com.example.security.demo.repository.postres.UserRepository;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

public class UserNameUniqueValidator implements ConstraintValidator<UserNameUnique, String> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void initialize(UserNameUnique contactNumber) { }

    @Override
    public boolean isValid(String userName,
                           ConstraintValidatorContext cxt) {
        if(userName == null) return false;
        System.out.println(userName);
        return !userRepository.existsByUsername(userName);
    }

}