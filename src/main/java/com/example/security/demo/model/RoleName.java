package com.example.security.demo.model;

public enum RoleName {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}