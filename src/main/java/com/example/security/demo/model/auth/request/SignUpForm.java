package com.example.security.demo.model.auth.request;

import com.example.security.demo.validation.EmailUnique;
import com.example.security.demo.validation.UserNameUnique;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SignUpForm {

    @NotBlank
    @Size(min = 3, max = 50)
    private String name;

    @NotBlank
    @Size(min = 3, max = 50)
    @UserNameUnique
    private String username;

    @EmailUnique
    @NotBlank
    @Size(max = 60)
    @Email
    private String email;

    private Set<String> roles = new HashSet<>();

    @NotBlank
    @Size(min = 6, max = 40)
    private String password;

    private String description;
}
