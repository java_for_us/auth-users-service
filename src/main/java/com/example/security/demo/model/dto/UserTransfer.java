package com.example.security.demo.model.dto;

import com.example.security.demo.model.Role;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserTransfer implements Serializable {
    private Long id;
    private String name;
    private String username;
    private String email;
    private Set<Role> roles = new HashSet<>();
}
