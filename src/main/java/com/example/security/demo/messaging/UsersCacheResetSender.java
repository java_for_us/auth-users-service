package com.example.security.demo.messaging;

import com.example.security.demo.config.UserCacheQueueConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsersCacheResetSender {
  private final RabbitTemplate rabbitTemplate;
  @Autowired
  public UsersCacheResetSender(RabbitTemplate rabbitTemplate) {
    this.rabbitTemplate = rabbitTemplate;
  }

  public void sendEventToClearCacheUsers() {
    this.rabbitTemplate.convertAndSend(UserCacheQueueConfig.USER_CACHE_QUEUE, "resetCache");
  }
}