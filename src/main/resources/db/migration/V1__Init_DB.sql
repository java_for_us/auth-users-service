create sequence hibernate_sequence start 1 increment 1;
create sequence users_id_seq start 2 increment 1;
create table roles
(
    id   bigint not null,
    name varchar(60),
    primary key (id)
);
create table user_roles
(
    user_id bigint not null,
    role_id bigint not null,
    primary key (user_id, role_id)
);
create table users
(
    id           bigint not null,
    created_date timestamp with time zone default now(),
    updated_date timestamp with time zone default now(),
    email        varchar(50),
    name         varchar(50),
    password     varchar(100),
    username     varchar(50),
    description  text,
    primary key (id)
);
alter table if exists roles
    add constraint UK_nb4h0p6txrmfc0xbrd1kglp9t unique (name);
alter table if exists users
    add constraint UKr43af9ap4edm43mmtq01oddj6 unique (username);
alter table if exists users
    add constraint UK6dotkott2kjsp8vw4d0m25fb7 unique (email);
alter table if exists user_roles
    add constraint FKh8ciramu9cc9q3qcqiv4ue8a6 foreign key (role_id) references roles;
alter table if exists user_roles
    add constraint FKhfh9dx7w3ubf1co1vdev94g3f foreign key (user_id) references users;